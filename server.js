var path = require("path")
var express = require("express")
//var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body

var app = express()  // make express app
var http = require('http').Server(app)  // inject app into the server

// 1 set up the view engine
app.set("views", path.resolve(__dirname, 'assets/')); // path to views
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html'); 

// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

app.use(express.static(__dirname + '/assets'))
// 3 set up an http request logger to log every request automagically
//app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
    response.render("index.html")
})

app.get("/Cart", function (request, response) {
    response.render("views/Cart.html")
})


app.get("/ContactUs", function (request, response) {
    response.render("views/ContactUs.html")
})

app.get("/Agarwal_Vineeth", function (request, response) {
    response.render("views/Agarwal_Vineeth.html")
})
app.get("/earrings", function (request, response) {
    response.render("views/earrings.html")
})
app.get("/bangles", function (request, response) {
    response.render("views/bangles.html")
})
app.get("/rings", function (request, response) {
    response.render("views/rings.html")
})
app.get("/necklaces", function (request, response) {
    response.render("views/necklaces.html")
})
app.get("/GuestBook", function (request, response) {
  response.render("views/guestbook.html")
})
app.get("/new-entry", function (request, response) {
  response.render("views/new-entry.html")
})

app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.")
    return
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  })
  response.redirect("/GuestBook")  // where to go next? Let's go to the home page :)
})

// 6 respond with 404 if a bad URI is requested
app.use(function (request, response) {
  response.status(404).render("404")
})

//Listen for an application request on port 8081 & notify the developer
app.listen(8081, function () {
console.log('Guestbook app listening on http://127.0.0.1:8081/')
})

